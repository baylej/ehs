# Electronic Hobbyist Simulator

What hobbyists do all the time?? They solder things!

Requirements:

- Linux, Windows or Mac OS.
- a gamepad with 2 sticks.

## Build

Requirements:

- [Allegro game programming library v 5.1.11+](http://liballeg.org)
- [CMake v3.0.x](http://cmake.org)
- A C compiler (gcc, clang, msvc)

### Run CMake

CMake is a build system generator, it can generate makefiles or project files.

You can use cmake or ccmake command line interfaces or cmake-gui.

If Allegro is not installed in a standard location, set the Alleg_ROOT variable
to its actual location.

### Build

Using your favourite build system / tools.

### Link/copy data dir

The `data/` directory should be in the same directory as the executable.

### Examples

Example with GCC and Make:

```
mkdir build && cd build && ln -s ../data/ data
cmake -G "Unix MakeFiles" -DAlleg_ROOT=/home/user/.local -DCMAKE_BUILD_TYPE=Release ..
make
./ehs
```

Example with MSVC and NMake

```
mkdir build & cd build & mklink /D data ..\data
cmake -G "NMake Makefiles" -DAlleg_ROOT=C:\Alleg\msvc\ -DCMAKE_BUILD_TYPE=Release ..
nmake
ehs.exe
```

