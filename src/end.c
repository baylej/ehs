#include "game.h"

#include <allegro5/allegro_primitives.h>

// sprite positions on spritesheet (end_sprite.png)
static aabb sunglasses = { .x= 0, .y= 0, .w=22, .h= 4 };
static aabb eyes       = { .x= 0, .y= 4, .w= 8, .h= 1 };
static aabb led        = { .x=22, .y= 0, .w= 8, .h= 8 };
static aabb smile      = { .x= 8, .y= 4, .w= 2, .h= 2 };
static aabb arm        = { .x= 0, .y= 9, .w=31, .h=16 };
static aabb smoke      = { .x=45, .y= 0, .w= 4, .h=21 };
static aabb explosion  = { .x=31, .y= 0, .w=13, .h=15 };
static aabb damages    = { .x= 0, .y=25, .w=28, .h=15 };

// draws/animate the endscene (character tries the circuit)
static void endscene(bool has_short) {
	ALLEGRO_EVENT ev;
	double start = al_get_time(), dtime;
	bool loop = true, played_boom = false, played_failure = false, played_success = false;

	point factor;
	float xoffset, left, top;

	aabb sg_dest, eyes_dest, arm_dest;
	float smile_dest_y;
	int smile_flag;

	int end_w = al_get_bitmap_width(endscene_bg),    end_h = al_get_bitmap_height(endscene_bg);

	al_flush_event_queue(ev_queue);
	do {
		al_wait_for_event(ev_queue, &ev);

		if (ev.type == ALLEGRO_EVENT_DISPLAY_RESIZE) {
			al_acknowledge_resize(display);
		}
		else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE ||
		  (ev.type == ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN && ev.joystick.id == gamepad &&
		  (ev.joystick.button == b_button.button || ev.joystick.button == a_button.button))) {
			loop = false;
		}
		else if (ev.type == ALLEGRO_EVENT_TIMER) {
			factor = get_screen_factor();
			xoffset = ((800 * factor.x) - (800 * factor.y)) / 2.;
			dtime = al_get_time() - start;

			al_clear_to_color(al_map_rgb(30,30,30));

			// Background
			left = xoffset + (800-8*end_w)/2. * factor.y;
			top  = (600-8*end_h) * factor.y;
			al_draw_scaled_bitmap(endscene_bg, 0, 0, end_w, end_h,
			  left, top, 8*end_w*factor.y, 8*end_h*factor.y, 0);

			// Eyes
			eyes_dest.w = eyes.w*8*factor.y;
			eyes_dest.h = eyes.h*8*factor.y;
			eyes_dest.x = left + ((dtime < 1. || dtime > 3.5)? 11: 12) *8*factor.y;
			eyes_dest.y = top  + 10*8*factor.y;
			al_draw_scaled_bitmap(endscene_sprites, eyes.x, eyes.y, eyes.w, eyes.h,
			  eyes_dest.x, eyes_dest.y, eyes_dest.w, eyes_dest.h, 0);

			// Arm
			arm_dest.w = arm.w*8*factor.y;
			arm_dest.h = arm.h*8*factor.y;
			arm_dest.x = left + 26*8*factor.y;
			arm_dest.y = top  + 23*8*factor.y;

			if (dtime < 3.) {
				// Arm
				al_draw_scaled_bitmap(endscene_sprites, arm.x, arm.y, arm.w, arm.h,
				  arm_dest.x, arm_dest.y, arm_dest.w, arm_dest.h, 0);
			}
			else {
				// Arm
				arm_dest.x += (arm.w/2. +.5) *8*factor.y;
				arm_dest.y += (arm.h/2. + 1) *8*factor.y;
				al_draw_tinted_scaled_rotated_bitmap_region(endscene_sprites,
				  arm.x, arm.y, arm.w, arm.h, al_map_rgba_f(1,1,1,1),
				  arm.w/2., arm.h/2., arm_dest.x, arm_dest.y, 8*factor.y, 8*factor.y, .05, 0);

				// Success
				if (!has_short) {
					// LED
					if (((int)(dtime*5))%2) {
						al_draw_scaled_bitmap(endscene_sprites, led.x, led.y, led.w, led.h,
						  left + 8*73 * factor.y, top + 8*52 * factor.y, 8*led.w*factor.y, 8*led.h*factor.y, 0);
					}
				}
				// Failure
				else {
					if (dtime < 3.1) {
						// Explosion
						al_draw_scaled_bitmap(endscene_sprites, explosion.x, explosion.y, explosion.w, explosion.h,
						  left + 8*76 * factor.y, top + 8*49 * factor.y,
						  8*explosion.w*factor.y, 8*explosion.h*factor.y, 0);
						if (!played_boom) {
							played_boom = true;
							al_play_sample(boom, .8, 0., 1., ALLEGRO_PLAYMODE_ONCE, NULL);
						}
					}
					else {
						// Damages
						al_draw_scaled_bitmap(endscene_sprites, damages.x, damages.y, damages.w, damages.h,
						  left + 8*71 * factor.y, top + 8*49 * factor.y,
						  8*damages.w*factor.y, 8*damages.h*factor.y, 0);
						// Smoke
						al_draw_scaled_bitmap(endscene_sprites, smoke.x, smoke.y, smoke.w, smoke.h,
						  left + 8*82 * factor.y, top + 8*26 * factor.y,
						  8*smoke.w*factor.y, 8*smoke.h*factor.y, 0);
					}
				}
			}

			if (dtime > 3.5) {
				if (!has_short) {
					// Smile
					smile_dest_y = top + 8*16*factor.y;
					smile_flag   = 0;

					// Sunglasses
					sg_dest.w = 8*sunglasses.w*factor.y;
					sg_dest.h = 8*sunglasses.h*factor.y;
					sg_dest.x = left;
					sg_dest.y = ((dtime < 6.5)? (dtime-3.5)/3.: 1.) * (top + 8*8*factor.y);
					al_draw_scaled_bitmap(endscene_sprites, sunglasses.x, sunglasses.y, sunglasses.w, sunglasses.h,
					  sg_dest.x, sg_dest.y, sg_dest.w, sg_dest.h, 0);

					if (!played_success) {
						al_play_sample(success, .8, 0., 1., ALLEGRO_PLAYMODE_ONCE, NULL);
						played_success = true;
					}
				}
				else {
					// Frown
					smile_dest_y = top + 8*18*factor.y;
					smile_flag   = ALLEGRO_FLIP_VERTICAL;
				}
				// Mouth
				al_draw_scaled_bitmap(endscene_sprites, smile.x, smile.y, smile.w, smile.h,
				  left + 8*12*factor.y, smile_dest_y,
				  8*smile.w*factor.y, 8*smile.h*factor.y, smile_flag);
			}

			if (dtime > 7.) {
				al_draw_filled_rectangle(0, 0, 800*factor.x, 600*factor.y, al_map_rgba(0,0,0,200));
				if (!has_short) {
					draw_scaled_text(font, al_map_rgb(255,255,255), xoffset + 400*factor.y, 300*factor.y, 3.,
					  ALLEGRO_ALIGN_CENTER, "Well Done!");
				}
				else {
					draw_scaled_text(font, al_map_rgb(255,255,255), xoffset + 400*factor.y, 300*factor.y, 3.,
					  ALLEGRO_ALIGN_CENTER, "You failed, BOO!");
					if (!played_failure) {
						al_play_sample(failure, .8, 0., 1., ALLEGRO_PLAYMODE_ONCE, NULL);
						played_failure = true;
					}
				}
			}

			if (dtime > 10.) {
				loop = false;
			}

			al_flip_display();
		}
	} while(loop);
}

static void draw_credits() {
	ALLEGRO_EVENT ev;
	double start = al_get_time();
	bool loop = true;

	point factor;
	float xoffset;

	al_flush_event_queue(ev_queue);
	do {
		al_wait_for_event(ev_queue, &ev);

		if (ev.type == ALLEGRO_EVENT_DISPLAY_RESIZE) {
			al_acknowledge_resize(display);
		}
		else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE ||
		  (ev.type == ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN && ev.joystick.id == gamepad &&
		  (ev.joystick.button == b_button.button || ev.joystick.button == a_button.button))) {
			loop = false;
		}
		else if (ev.type == ALLEGRO_EVENT_TIMER) {
			factor = get_screen_factor();
			xoffset = ((800 * factor.x) - (800 * factor.y)) / 2.;

			al_clear_to_color(al_map_rgb(0,0,0));

			draw_scaled_text(font, al_map_rgb(255,255,255), xoffset + 400*factor.y, 75*factor.y,
			  6.*factor.y, ALLEGRO_ALIGN_CENTER, "Credits");

			draw_scaled_text(font, al_map_rgb(255,255,255), xoffset +  25*factor.y, 200*factor.y,
			  4.*factor.y, 0, "Code, Graphics");

			draw_scaled_text(font, al_map_rgb(255,255,255), xoffset +  50*factor.y, 250*factor.y,
			  2.5*factor.y, 0, "Jonathan aka Mr-Hide (@mrhidefr)");

			draw_scaled_text(font, al_map_rgb(255,255,255), xoffset +  25*factor.y, 300*factor.y,
			  4.*factor.y, 0, "Voice Actors");

			draw_scaled_text(font, al_map_rgb(255,255,255), xoffset +  50*factor.y, 350*factor.y,
			  2.5*factor.y, 0, "Dean aka Marcadet (@Evil_Dean)");

			draw_scaled_text(font, al_map_rgb(255,255,255), xoffset +  50*factor.y, 375*factor.y,
			  2.5*factor.y, 0, "Nathan aka Tmnath (@Tmnath)");

			draw_scaled_text(font, al_map_rgb(255,255,255), xoffset +  25*factor.y, 425*factor.y,
			  4.*factor.y, 0, "Electronic Expert");

			draw_scaled_text(font, al_map_rgb(255,255,255), xoffset +  50*factor.y, 475*factor.y,
			  2.5*factor.y, 0, "Vincent aka TDK (@TueurDeTwingo)");

			al_flip_display();

			if (al_get_time() - start > 10.) {
				loop = false;
			}
		}
	} while (loop);
}

void game_over(bool is_dead) {
	point factor = get_screen_factor();
	float xoffset = ((800 * factor.x) - (800 * factor.y)) / 2.;

	if (is_dead) {
		al_clear_to_color(al_map_rgb(0,0,0));
		int dead_bg_h = al_get_bitmap_height(dead_bg),    dead_bg_w = al_get_bitmap_width(dead_bg);
		al_draw_scaled_bitmap(dead_bg, 0, 0, dead_bg_w, dead_bg_h,
		  xoffset + (800-8*dead_bg_w)/2. * factor.y, (600-8*dead_bg_h)*factor.y,
		  8*dead_bg_w*factor.y, 8*dead_bg_h*factor.y, 0);
		al_draw_filled_rectangle(0, 0, 800*factor.x, 600*factor.y, al_map_rgba(0,0,0,200));
		draw_scaled_text(font, al_map_rgb(255,255,255), xoffset + 400*factor.y, 300*factor.y,
		  3.*factor.y, ALLEGRO_ALIGN_CENTER, "You killed yourself ...");
		al_flip_display();
		al_rest(1.0);
		al_play_sample(dead, .8, 0., 1., ALLEGRO_PLAYMODE_ONCE, NULL);
		al_rest(2.5);
		return;
	}

	double dt = al_get_time();
	bool has_short = check_for_shorts(); // TODO rename to is_toto
	dt = al_get_time() - dt;
	printlogf("check_for_shorts() done in %lfms\n", dt*1000);

	stop_music();
	endscene(has_short);

	if (!has_short) {
		draw_credits();
	}
}

