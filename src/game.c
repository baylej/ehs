#include "game.h"

#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_color.h>

///     GEOMETRY

const segment left_hand_contact = {
  .A = { .x= 80, .y=780 },
  .B = { .x=280, .y=320 }
};
const segment iron_contact = {
  .A = { .x=770, .y=520 },
  .B = { .x=512, .y=264 }
};

static bool is_hurt = false;
static double last_hurt = 0.;
static int health = 3;

///     INPUT

static float left_h, left_v, right_h, right_v;

static bool pressed[32];

// Applying the iron on a joint and move to another cell makes the joint an overflowing joint
static point iron_origin_cell;
static bool can_overflow = false;

// stick values to screen position (without the 75,20 px margin)
static inline point left_stick_screen_position() {
	point res;
	res.x = GP_LEFT_M +  left_h * GP_WIDTH;
	res.y = GP_TOP_M  +  left_v * GP_HEIGHT;
	return res;
}

// stick values to screen position (without the 75,20 px margin)
static inline point right_stick_screen_position() {
	point res;
	res.x = GP_RIGHT_M + right_h * GP_WIDTH;
	res.y = GP_TOP_M   + right_v * GP_HEIGHT;
	return res;
}

// returns coordinates of the cell being rendered at given position, may returns out of bounds values
static inline point get_cell_at_screen_position(point screen_pos) {
	int pcb_h = al_get_bitmap_height(current_level.pcb),   pcb_w = al_get_bitmap_width(current_level.pcb);
	float pcb_hf = pcb_h * current_level.pcb_f,   pcb_wf = pcb_w * current_level.pcb_f;
	float cell_h = pcb_hf / current_level.h,      cell_w = pcb_wf / current_level.w;
	point res;
	res.x = (int) ((screen_pos.x - (GA_W - pcb_wf)/2.) / cell_w);
	res.y = (int) ((screen_pos.y - (GA_H - pcb_hf)/2.) / cell_h);
	return res;
}

// Returns `true` if both left_bottom_button, right_bottom_button
static inline bool both_pressed() {
	return pressed[left_bottom_button.button] && pressed[right_bottom_button.button];
}

// Sets values of left_h, left_v, right_h and right_v
static void set_stick_values() {
	ALLEGRO_JOYSTICK_STATE gamepad_state;
	al_get_joystick_state(gamepad, &gamepad_state);

	left_h  = gamepad_state.stick[ left_stick_h.stick].axis[ left_stick_h.axis];
	left_v  = gamepad_state.stick[ left_stick_v.stick].axis[ left_stick_v.axis];

	right_h = gamepad_state.stick[right_stick_h.stick].axis[right_stick_h.axis];
	right_v = gamepad_state.stick[right_stick_v.stick].axis[right_stick_v.axis];
}

///     DRAWING

struct tile_info {
	int x_off, y_off; // x and y offset of tile to draw
	int flags; // flag (ALLEGRO_FLIP_HORIZONTAL | ALLEGRO_FLIP_VERTICAL)
};

// Corner index for `get_corners_for_cell`
enum corner { TOP_LEFT_CORNER, TOP_RIGHT_CORNER, BOTTOM_LEFT_CORNER, BOTTOM_RIGHT_CORNER };

// Returns a tile_info for each corner of the given cell
static unsigned char get_corners_for_cell(int cell_x, int cell_y, struct tile_info corners[4]) {
	unsigned char res = 0;

	int mask = (get_cell(cell_x, cell_y) & OVERFLOW) ? JOINT: OVERFLOW;

	int  top       =  get_cell(cell_x,   cell_y-1) & mask;
	int  left      =  get_cell(cell_x-1, cell_y)   & mask;
	int  right     =  get_cell(cell_x+1, cell_y)   & mask;
	int  bottom    =  get_cell(cell_x,   cell_y+1) & mask;
	bool top_left  = (get_cell(cell_x-1, cell_y-1) & OVERFLOW) | (OVERFLOW & (   top | left));
	bool top_right = (get_cell(cell_x+1, cell_y-1) & OVERFLOW) | (OVERFLOW & (   top | right));
	bool bot_left  = (get_cell(cell_x-1, cell_y+1) & OVERFLOW) | (OVERFLOW & (bottom | left));
	bool bot_right = (get_cell(cell_x+1, cell_y+1) & OVERFLOW) | (OVERFLOW & (bottom | right));

	// Top-Left corner
	if (top && left) {
		if (top_left) {
			corners[TOP_LEFT_CORNER].x_off = 24;
			corners[TOP_LEFT_CORNER].y_off = 8;
			corners[TOP_LEFT_CORNER].flags = 0;
		}
		else {
			corners[TOP_LEFT_CORNER].x_off = 0;
			corners[TOP_LEFT_CORNER].y_off = 16;
			corners[TOP_LEFT_CORNER].flags = 0;
		}
	}
	else if (top) {
		corners[TOP_LEFT_CORNER].x_off = 16;
		corners[TOP_LEFT_CORNER].y_off = 0;
		corners[TOP_LEFT_CORNER].flags = 0;
	}
	else if (left) {
		corners[TOP_LEFT_CORNER].x_off = 24;
		corners[TOP_LEFT_CORNER].y_off = 0;
		corners[TOP_LEFT_CORNER].flags = 0;
	}
	else {
		corners[TOP_LEFT_CORNER].x_off = 0;
		corners[TOP_LEFT_CORNER].y_off = 0;
		corners[TOP_LEFT_CORNER].flags = 0;
	}

	// Top-Right corner
	if (top && right) {
		if (top_right) {
			corners[TOP_RIGHT_CORNER].x_off = 24;
			corners[TOP_RIGHT_CORNER].y_off = 8;
			corners[TOP_RIGHT_CORNER].flags = 0;
		}
		else {
			corners[TOP_RIGHT_CORNER].x_off = 0;
			corners[TOP_RIGHT_CORNER].y_off = 16;
			corners[TOP_RIGHT_CORNER].flags = ALLEGRO_FLIP_HORIZONTAL;
		}
	}
	else if (top) {
		corners[TOP_RIGHT_CORNER].x_off = 16;
		corners[TOP_RIGHT_CORNER].y_off = 0;
		corners[TOP_RIGHT_CORNER].flags = ALLEGRO_FLIP_HORIZONTAL;
	}
	else if (right) {
		corners[TOP_RIGHT_CORNER].x_off = 24;
		corners[TOP_RIGHT_CORNER].y_off = 0;
		corners[TOP_RIGHT_CORNER].flags = ALLEGRO_FLIP_HORIZONTAL;
	}
	else {
		corners[TOP_RIGHT_CORNER].x_off = 0;
		corners[TOP_RIGHT_CORNER].y_off = 0;
		corners[TOP_RIGHT_CORNER].flags = ALLEGRO_FLIP_HORIZONTAL;
	}

	// Bottom-Left corner
	if (bottom && left) {
		if (bot_left) {
			corners[BOTTOM_LEFT_CORNER].x_off = 24;
			corners[BOTTOM_LEFT_CORNER].y_off = 8;
			corners[BOTTOM_LEFT_CORNER].flags = 0;
		}
		else {
			corners[BOTTOM_LEFT_CORNER].x_off = 0;
			corners[BOTTOM_LEFT_CORNER].y_off = 24;
			corners[BOTTOM_LEFT_CORNER].flags = 0;
		}
	}
	else if (bottom) {
		corners[BOTTOM_LEFT_CORNER].x_off = 16;
		corners[BOTTOM_LEFT_CORNER].y_off = 0;
		corners[BOTTOM_LEFT_CORNER].flags = 0;
	}
	else if (left) {
		corners[BOTTOM_LEFT_CORNER].x_off = 16;
		corners[BOTTOM_LEFT_CORNER].y_off = 8;
		corners[BOTTOM_LEFT_CORNER].flags = 0;
	}
	else {
		corners[BOTTOM_LEFT_CORNER].x_off = 0;
		corners[BOTTOM_LEFT_CORNER].y_off = 8;
		corners[BOTTOM_LEFT_CORNER].flags = 0;
	}

	// Bottom-Right corner
	if (bottom && right) {
		if (bot_right) {
			corners[BOTTOM_RIGHT_CORNER].x_off = 24;
			corners[BOTTOM_RIGHT_CORNER].y_off = 8;
			corners[BOTTOM_RIGHT_CORNER].flags = 0;
		}
		else {
			corners[BOTTOM_RIGHT_CORNER].x_off = 0;
			corners[BOTTOM_RIGHT_CORNER].y_off = 24;
			corners[BOTTOM_RIGHT_CORNER].flags = ALLEGRO_FLIP_HORIZONTAL;
		}
	}
	else if (bottom) {
		corners[BOTTOM_RIGHT_CORNER].x_off = 16;
		corners[BOTTOM_RIGHT_CORNER].y_off = 0;
		corners[BOTTOM_RIGHT_CORNER].flags = ALLEGRO_FLIP_HORIZONTAL;
	}
	else if (right) {
		corners[BOTTOM_RIGHT_CORNER].x_off = 16;
		corners[BOTTOM_RIGHT_CORNER].y_off = 8;
		corners[BOTTOM_RIGHT_CORNER].flags = 0;
	}
	else {
		corners[BOTTOM_RIGHT_CORNER].x_off = 0;
		corners[BOTTOM_RIGHT_CORNER].y_off = 8;
		corners[BOTTOM_RIGHT_CORNER].flags = ALLEGRO_FLIP_HORIZONTAL;
	}

	return res;
}

static void draw_game() {
	point factor = get_screen_factor();
	float xoffset = ((800*factor.x) - (800*factor.y)) / 2.;

	// game area = 650x480 px with a 150px horizontal margin and a 20px vertical top margin
	float left = xoffset + 75*factor.y;
	float top = 20*factor.y;

	al_clear_to_color(al_map_rgb(0,0,0));

	al_draw_filled_rectangle(left, top, left+650*factor.y, top+480*factor.y, al_map_rgb(80,80,80));

	// Draws the PCB
	int    pcb_h = al_get_bitmap_height(current_level.pcb),   pcb_w = al_get_bitmap_width(current_level.pcb);
	float pcb_hf = pcb_h * current_level.pcb_f * factor.y,   pcb_wf = pcb_w * current_level.pcb_f * factor.y;
	float pcbleft = left + (650 - pcb_w*current_level.pcb_f)/2. * factor.y;
	float pcbtop  =  top + (480 - pcb_h*current_level.pcb_f)/2. * factor.y;

	al_draw_scaled_bitmap(current_level.pcb, 0, 0, pcb_w, pcb_h, pcbleft, pcbtop, pcb_wf, pcb_hf, 0);

	float cell_h = pcb_hf / (float)current_level.h,   cell_w = pcb_wf / (float)current_level.w;
	int i, j;
#ifndef NDEBUG
	// Draws a grid in debug mode
	for (i=0; i<current_level.w; i++) {
		al_draw_line(pcbleft + i*cell_w, pcbtop, pcbleft + i*cell_w, pcbtop + pcb_hf, al_map_rgb(255,0,0), 1.);
	}
	for (i=0; i<current_level.h; i++) {
		al_draw_line(pcbleft, pcbtop + i*cell_h, pcbleft + pcb_wf, pcbtop + i*cell_h, al_map_rgb(255,0,0), 1.);
	}
	// Highlights holes
	for (i=0; i<current_level.holes_len; i++) {
		al_draw_circle(pcbleft + current_level.holes[i][0] * cell_w + cell_w/2.,
		  pcbtop + current_level.holes[i][1] * cell_h+ cell_h/2., cell_w/2.5, al_map_rgb(120,120,200), 2. * factor.y);
	}
#endif

	float cell_h2 = cell_h / 2.,   cell_w2 = cell_w / 2.;

	// Draws joints
	struct tile_info tileinfos[4];
	for (i=0; i<current_level.w; i++) {
		for (j=0; j<current_level.h; j++) {
			if (*(current_level.grid + i + j*current_level.w) & JOINT) {
				float joint_left = pcbleft + i*cell_w,   joint_top = pcbtop + j*cell_h;
				get_corners_for_cell(i, j, tileinfos);
				al_draw_scaled_bitmap(joints,
				  tileinfos[TOP_LEFT_CORNER].x_off, tileinfos[TOP_LEFT_CORNER].y_off, 8, 8,
				  joint_left, joint_top, cell_w2, cell_h2, tileinfos[TOP_LEFT_CORNER].flags);
				al_draw_scaled_bitmap(joints,
				  tileinfos[TOP_RIGHT_CORNER].x_off, tileinfos[TOP_RIGHT_CORNER].y_off, 8, 8,
				  joint_left + cell_w2, joint_top, cell_w2, cell_h2, tileinfos[TOP_RIGHT_CORNER].flags);
				al_draw_scaled_bitmap(joints,
				  tileinfos[BOTTOM_LEFT_CORNER].x_off, tileinfos[BOTTOM_LEFT_CORNER].y_off, 8, 8,
				  joint_left, joint_top + cell_h2, cell_w2, cell_h2, tileinfos[BOTTOM_LEFT_CORNER].flags);
				al_draw_scaled_bitmap(joints,
				  tileinfos[BOTTOM_RIGHT_CORNER].x_off, tileinfos[BOTTOM_RIGHT_CORNER].y_off, 8, 8,
				  joint_left + cell_w2, joint_top + cell_h2, cell_w2, cell_h2, tileinfos[BOTTOM_RIGHT_CORNER].flags);
			}
			// Draws leads
			else if (*(current_level.grid + i + j*current_level.w) & HOLE) {
				float lead_left = pcbleft + i*cell_w + cell_w/2.,   lead_top = pcbtop + j*cell_h + cell_h/2. + cell_h/10.;
				al_draw_line(lead_left, lead_top, lead_left, lead_top - cell_h/2., al_map_rgb(150,150,150), 6.*factor.y);
			}

#ifndef NDEBUG
			if (*(current_level.grid + i + j*current_level.w) & OVERFLOW) {
				al_draw_filled_circle(pcbleft + i*cell_w + cell_w/2.,    pcbtop + j*cell_h+ cell_h/2.,
				  cell_w/4., al_map_rgb(120,30,30));
			}
#endif

		}
	}

	// Draws hands
	// draw hands, spawn location is at one third of the width and at mid-height of the game area
	// they can move within a 375px range horizontally, and a 240px range vertically
	point hand_right = right_stick_screen_position();
	float hand_right_left = left + hand_right.x * factor.y,   hand_right_top = top + hand_right.y * factor.y;
	float hand_right_top_hover = hand_right_top;
	if (!pressed[right_bottom_button.button]) {
		hand_right_top_hover -= SHADOW_HOVER * factor.y;
	}

	float hand_right_w = al_get_bitmap_width(right_hand),    hand_right_fw = hand_right_w * 4. * factor.y;
	float hand_right_h = al_get_bitmap_height(right_hand),   hand_right_fh = hand_right_h * 4. * factor.y;

	al_draw_scaled_bitmap(right_hand_shadow, 0, 0, hand_right_w, hand_right_h,
	  hand_right_left, hand_right_top, hand_right_fw, hand_right_fh, 0);

	al_draw_scaled_bitmap(right_hand, 0, 0, hand_right_w, hand_right_h,
	  hand_right_left, hand_right_top_hover, hand_right_fw, hand_right_fh, 0);

	point hand_left = left_stick_screen_position();
	float hand_left_left = left + hand_left.x * factor.y,   hand_left_top = top + hand_left.y * factor.y;
	float hand_left_top_hover = hand_left_top;
	if (!pressed[left_bottom_button.button]) {
		hand_left_top_hover -= SHADOW_HOVER * factor.y;
	}

	float hand_left_w = al_get_bitmap_width(left_hand),    hand_left_fw = hand_left_w * 4. * factor.y;
	float hand_left_h = al_get_bitmap_height(left_hand),   hand_left_fh = hand_left_h * 4. * factor.y;

	// draws the solder wire before the hand
	al_draw_line(hand_left_left, hand_left_top_hover + 4*factor.y,
	  hand_left_left - 12*4*factor.y,    hand_left_top_hover + 4*factor.y + 12*4*factor.y,
	  al_map_rgb(234,231,218), 8.0 * factor.y);

	al_draw_scaled_bitmap(left_hand_shadow, 0, 0, hand_left_w, hand_left_h,
	  hand_left_left - hand_left_fw, hand_left_top, hand_left_fw, hand_left_fh, 0);

	al_draw_scaled_bitmap(left_hand, 0, 0, hand_left_w, hand_left_h,
	  hand_left_left - hand_left_fw, hand_left_top_hover, hand_left_fw, hand_left_fh, 0);

#ifndef NDEBUG
	al_draw_circle(hand_right_left, hand_right_top, 6, al_map_rgb(220,40,90), 2);
	al_draw_circle(hand_left_left, hand_left_top, 6, al_map_rgb(90,220,40), 2);

	point vect;
	vect.x = left_h * GP_WIDTH;
	vect.y = left_v * GP_HEIGHT;
	segment pos = translate(&left_hand_contact, &vect);
	al_draw_line(xoffset+pos.A.x*factor.y, pos.A.y*factor.y, xoffset+pos.B.x*factor.y, pos.B.y*factor.y,
	  al_map_rgb(150,120,180), 3.);

	vect.x = right_h * GP_WIDTH;
	vect.y = right_v * GP_HEIGHT;
	pos = translate(&iron_contact, &vect);
	al_draw_line(xoffset+pos.A.x*factor.y, pos.A.y*factor.y, xoffset+pos.B.x*factor.y, pos.B.y*factor.y,
	  al_map_rgb(100,170,140), 3.);
#endif

	// UI (objectives)
	al_draw_textf(font, al_map_rgb(255,255,255), xoffset+400*factor.y, 560*factor.y, ALLEGRO_ALIGN_CENTER,
	  "%d/%d joints", current_level.holes_done, current_level.holes_len);

	if (is_hurt) {
		al_draw_filled_rectangle(0, 0, 800*factor.x, 600*factor.y, al_map_rgba(180,0,0,120));
	}

	int heart_w = al_get_bitmap_width(heart),   heart_h = al_get_bitmap_height(heart);
	for (i=0; i<health; i++) {
		al_draw_scaled_bitmap(heart, 0, 0, heart_w, heart_h,
		  xoffset + (740 + i*(heart_w+2)*3)*factor.y, 20*factor.y,
		  3 * heart_w*factor.y, 3 * heart_h*factor.y, 0);
	}

#ifndef NDEBUG
	if (al_is_mouse_installed()) {
		ALLEGRO_MOUSE_STATE mstate;
		al_get_mouse_state(&mstate);
		int drx = mstate.x-12,   dry = mstate.y-12;
		int flag = ALLEGRO_ALIGN_RIGHT;
		if (mstate.x < 100) {
			drx = mstate.x+12;
			flag = 0;
		}
		if (mstate.y < 20) {
			dry = mstate.y+12;
		}
		al_draw_textf(font, al_map_rgb_f(1,1,1), drx, dry, flag,
		  "%d,%d", (int) (mstate.x/factor.y - xoffset), (int) (mstate.y/factor.y));
	}
#endif
}

//  0 <= dtime <= 1
static void draw_instructions(double dtime) {
	point factor = get_screen_factor();
	float xoffset = ((800*factor.x) - (800*factor.y)) / 2.;
	ALLEGRO_COLOR col = al_color_hsv(300.f*dtime,.5,.8);

	draw_scaled_text(font, col, xoffset + 400*factor.y,  80*factor.y, 3.,
	  ALLEGRO_ALIGN_CENTER, "Use sticks to move hands");
	draw_scaled_text(font, col, xoffset + 400*factor.y, 110*factor.y, 3.,
	  ALLEGRO_ALIGN_CENTER, "Use LB and RB to apply tin and iron");
}

static void draw_game_is_about_to_end() {
	point factor = get_screen_factor();
	float xoffset = ((800*factor.x) - (800*factor.y)) / 2.;

	al_draw_filled_rectangle(0, 0, 800*factor.x, 600*factor.y, al_map_rgba(0,0,0,200));
	draw_scaled_text(font, al_map_rgb(255,255,255), xoffset + 400*factor.y, 300*factor.y, 3., ALLEGRO_ALIGN_CENTER,
	  "I'm done with this, let's try it!");
}

// creates a joint and returns `true` if and only if all condition are met to create an joint
static bool test_and_set_joint() {
	point lh =  left_stick_screen_position();
	point rh = right_stick_screen_position();

	// res is true if both hands are in ⌀25px area
	bool res = abs(rh.x - lh.x) < 18  &&  abs(rh.y - lh.y) < 18;

	if (res) {
		// calculates the corresponding cell position
		point cell = get_cell_at_screen_position(rh);

		// add the joint mask to the cell
		if (cell.x >= 0 && cell.x < current_level.w &&
		    cell.y >= 0 && cell.y < current_level.h ) {

			int index = cell.x + cell.y*current_level.w;

			if (current_level.grid[index] & HOLE && !(current_level.grid[index] & JOINT)) {
				current_level.holes_done++;
			}

			if (current_level.grid[index] & JOINT) {
				current_level.grid[index] |= OVERFLOW;
			}
			else {
				current_level.grid[index] |= JOINT;
				can_overflow = true;
			}
		}
	}

	return res;
}

static bool are_we_done_yet() {
	return current_level.holes_done == current_level.holes_len;
}

// sets global var `is_hurt`
static void check_if_we_hurt_ourselve() {
	point vect;

	vect.x = left_h * GP_WIDTH;
	vect.y = left_v * GP_HEIGHT;
	segment leftpos = translate(&left_hand_contact, &vect);

	vect.x = right_h * GP_WIDTH;
	vect.y = right_v * GP_HEIGHT;
	segment rightpos = translate(&iron_contact, &vect);

	is_hurt = intersect(&rightpos, &leftpos);

	if (is_hurt && al_get_time() - last_hurt > 1.5) {
		health--;
		al_play_sample(hurt, .8, 0., 1., ALLEGRO_PLAYMODE_ONCE, NULL);
		last_hurt = al_get_time();
	}
}

// sets Joints and Overflow flags to overflowed and source cells
static void check_if_we_overflowed() {
	point new_pos = get_cell_at_screen_position(right_stick_screen_position());

	if (pressed[right_bottom_button.button]) {
		if (can_overflow) {
			if (new_pos.x != iron_origin_cell.x && new_pos.x >= 0 && new_pos.x < current_level.w) {
				int cellv = get_cell(new_pos.x, iron_origin_cell.y);
				if (!(cellv & JOINT) && cellv & HOLE) {
					current_level.holes_done++;
				}
				cellv |= (cellv & JOINT) ? OVERFLOW : JOINT;
				set_cell(new_pos.x, iron_origin_cell.y, cellv);
				set_cell(iron_origin_cell.x, iron_origin_cell.y, get_cell(iron_origin_cell.x, iron_origin_cell.y) | OVERFLOW);
				iron_origin_cell.x = new_pos.x;
			}
			else if (new_pos.y != iron_origin_cell.y && new_pos.y >= 0 && new_pos.y < current_level.h) {
				int cellv = get_cell(iron_origin_cell.x, new_pos.y);
				if (!(cellv & JOINT) && cellv & HOLE) {
					current_level.holes_done++;
				}
				cellv |= (cellv & JOINT) ? OVERFLOW : JOINT;
				set_cell(iron_origin_cell.x, new_pos.y, cellv);
				set_cell(iron_origin_cell.x, iron_origin_cell.y, get_cell(iron_origin_cell.x, iron_origin_cell.y) | OVERFLOW);
				iron_origin_cell.y = new_pos.y;
			}
		}
		else if (get_cell(new_pos.x, new_pos.y) & JOINT) {
			can_overflow = true;
			iron_origin_cell.x = new_pos.x;
			iron_origin_cell.y = new_pos.y;
		}
	}
}

bool game() {
	bool continue_mainloop = true;
	ALLEGRO_EVENT ev;
	double lr_s = al_get_time(); // last redraw time in seconds
	double start = lr_s;
	double d_s; // delta times between redraws in seconds
	double done_start = 0.;

	health = 3;
	memset(pressed, 0, 32*sizeof(bool));
	can_overflow = false;

	play_music();

	al_flush_event_queue(ev_queue);
	do {
		al_wait_for_event(ev_queue, &ev);
		if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
			continue_mainloop = false;
		}
		else if (ev.type == ALLEGRO_EVENT_DISPLAY_RESIZE) {
			al_acknowledge_resize(display);
		}
		else if (ev.type == ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN && ev.joystick.id == gamepad) {
			pressed[ev.joystick.button] = true;
			set_stick_values();
			if (ev.joystick.button == right_bottom_button.button) {
				iron_origin_cell = get_cell_at_screen_position(right_stick_screen_position());
				if (get_cell(iron_origin_cell.x, iron_origin_cell.y) & JOINT) {
					can_overflow = true;
				}
			}
			if (both_pressed()) {
				test_and_set_joint();
			}
		}
		else if (ev.type == ALLEGRO_EVENT_JOYSTICK_BUTTON_UP && ev.joystick.id == gamepad) {
			pressed[ev.joystick.button] = false;
			if (ev.joystick.button == right_bottom_button.button) {
				can_overflow = false;
			}
		}
		else if (ev.type == ALLEGRO_EVENT_KEY_DOWN && ev.keyboard.keycode == ALLEGRO_KEY_F8) {
			game_over(false);
			al_flush_event_queue(ev_queue);
		}
		else if (ev.type == ALLEGRO_EVENT_KEY_DOWN && ev.keyboard.keycode == ALLEGRO_KEY_F10) {
			for (int i=0; i<current_level.holes_len; i++) {
				*(current_level.grid + current_level.holes[i][0] + current_level.holes[i][1]*current_level.w) |= JOINT;
			}
			current_level.holes_done = current_level.holes_len;
		}
		else if (ev.type == ALLEGRO_EVENT_TIMER) {
			set_stick_values();
			check_if_we_hurt_ourselve();
			check_if_we_overflowed();
			draw_game();

			d_s = lr_s;
			lr_s = al_get_time();
			d_s = lr_s - d_s;
			al_draw_textf(font, al_map_rgb(255,255,255), 10, 10, 0, "%4d FPS", (int) (1./ d_s));

			d_s = lr_s - start;
			if (d_s < 5.) {
				draw_instructions(d_s/5.);
			}

			if (are_we_done_yet()) {
				if (done_start == 0.) {
					done_start = al_get_time();
				}
				d_s = lr_s - done_start;
				if (d_s < 2.) {
					draw_game_is_about_to_end();
				}
				else {
					game_over(false);
					continue_mainloop = false;
				}
			}

			al_flip_display();

			if (health <= 0) {
				game_over(true);
				continue_mainloop = false;
			}
		}
	} while (continue_mainloop);

	al_flush_event_queue(ev_queue);
	stop_music();
	return true;
}

