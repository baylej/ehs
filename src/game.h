#ifndef GAME_H
#define GAME_H
#pragma once

#include <stdlib.h>
#include <stdbool.h>
#include <stdarg.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_native_dialog.h>
#include <allegro5/allegro_audio.h>

#define APP_NAME "Electronic Hobbyist Simulator"
#define FPS 60.0
#define RESOURCES_DIR "data"

extern ALLEGRO_DISPLAY *display;
extern ALLEGRO_TIMER *timer;
extern ALLEGRO_EVENT_QUEUE *ev_queue;
extern ALLEGRO_PATH *config_path;
extern ALLEGRO_CONFIG *config;
extern ALLEGRO_FONT *font;
extern ALLEGRO_JOYSTICK *gamepad;

extern ALLEGRO_BITMAP *cursor;
extern ALLEGRO_BITMAP *menu_bg;
extern ALLEGRO_BITMAP *conf_gamepad;
extern ALLEGRO_BITMAP *heart;

extern ALLEGRO_BITMAP *left_hand;
extern ALLEGRO_BITMAP *right_hand;
extern ALLEGRO_BITMAP *left_hand_shadow;
extern ALLEGRO_BITMAP *right_hand_shadow;
extern ALLEGRO_BITMAP *joints;
extern ALLEGRO_BITMAP *background;

extern ALLEGRO_BITMAP *dead_bg;
extern ALLEGRO_BITMAP *endscene_bg;
extern ALLEGRO_BITMAP *endscene_sprites;

extern ALLEGRO_AUDIO_STREAM *music;

extern ALLEGRO_SAMPLE *hurt;
extern ALLEGRO_SAMPLE *dead;
extern ALLEGRO_SAMPLE *boom;
extern ALLEGRO_SAMPLE *failure;
extern ALLEGRO_SAMPLE *success;

extern ALLEGRO_TEXTLOG *textlog;

// Bindings structure definition
struct button {
	int joy, button; // indices
};

struct axis {
	int joy, stick, axis; // indices
};

// Joystick bindings indices
extern  struct axis    left_stick_v, left_stick_h, right_stick_v, right_stick_h;
extern  struct button  left_bottom_button, right_bottom_button, a_button, b_button;

// Binding names in config
extern const char *LEFT_STICK_V, *LEFT_STICK_H, *RIGHT_STICK_V, *RIGHT_STICK_H;
extern const char *LEFT_BOTTOM_BUTTON, *RIGHT_BOTTOM_BUTTON, *A_BUTTON, *B_BUTTON;

typedef struct {
	float x, y;
} point;

typedef struct {
	unsigned int x, y, w, h; // Xoffset, Yoffset, Width, Height
} aabb; // Axis Aligned Bounding Box

typedef struct {
	point A, B;
} segment;

extern const segment left_hand_contact;
extern const segment iron_contact;

// util.c
// utilities
/// displays a `message` dialog, always available
void show_error(const char *errmsg);
/// prints to `textlog` if available, or to `stdout`
void printlog(const char *message);
void printlogf(const char *format, ...);
void vprintlogf(const char *format, va_list varargs);
/// loads given filename from `ALLEGRO_RESOURCES_PATH/RESOURCE_DIR` as image
ALLEGRO_BITMAP* get_resource_image(const char *filename);
/// loads given filename from `ALLEGRO_RESOURCES_PATH/RESOURCE_DIR` as a sample
ALLEGRO_SAMPLE* get_resource_sample(const char *filename);
/// loads given filename from `ALLEGRO_RESOURCES_PATH/RESOURCE_DIR` as a stream
ALLEGRO_AUDIO_STREAM* get_resource_stream(const char *filename);
/// save button binding config
void config_set_bind_button(const char *name, struct button values);
/// save axis binding config
void config_set_bind_axis(const char *name, struct axis values);
/// al_draw_ustr with a scale parameter
void draw_scaled_ustr(const ALLEGRO_FONT *font, ALLEGRO_COLOR color,
	float x, float y, float scale, int flags, const ALLEGRO_USTR *ustr);
/// al_draw_text with a scale parameter
void draw_scaled_text(const ALLEGRO_FONT *font, ALLEGRO_COLOR color,
	float x, float y, float scale, int flags, const char *text);
/// Returns factor of boundaries of the current display compared to the standard 800x600
point get_screen_factor();
/// Plays/Stops music stream
void play_music();
void stop_music();

// init.c
// initialises and loads most things
bool init(void);
void deinit(void);

// game.c
// game logic
bool game(void);
/// Game area is 650*480px
#define GA_W 650
#define GA_H 480
#define GA_PAD_W 40
#define GA_PAD_H 10

/// GamePad sticks' width, height and margins
#define GP_WIDTH  400
#define GP_HEIGHT 240
#define GP_TOP_M   240
#define GP_LEFT_M  215
#define GP_RIGHT_M 435

#define SHADOW_HOVER 12

// menu.c
// menus
void main_menu(void);
void config_gamepad_menu(void);
bool select_level(void);

// level.c
typedef struct _level_data {
	int w, h; // Width, Height of the grid
	int *grid; // Grid, initilised to 0, set to 1 if contains solder
	int (*holes)[2]; // Holes positions on the grid
	int holes_len; // length of `holes`
	int holes_done; // count holes having their joint
	ALLEGRO_BITMAP *pcb; // background image
	float pcb_f; // factor to apply to the pcb so then it fits in the game are minus padding
} level_data;
extern level_data current_level;

#define    TRACK 0x01
#define    JOINT 0x02
#define     HOLE 0x04
#define OVERFLOW 0x08

bool load_level(int index);
void destroy_level();
/// returns the content of `current_level.grid`, boundaries aware (no segfault, returns 0 as default value)
int get_cell(int cell_x, int cell_y);
// sets the content of `current_level.grid`, boundaries aware (no segfault)
void set_cell(int cell_x, int cell_y, int val);
bool check_for_shorts();

// math.c
/// translate segment `s` by vector `v`
segment translate(const segment *s, const point *v);
/// returns `true' if s1 and s2 intersect
bool intersect(const segment *s1, const segment *s2);

// end.c
void game_over(bool is_dead);

#endif // GAME_H