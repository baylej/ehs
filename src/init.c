#include "game.h"

#include <stdlib.h>
#include <stdio.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_acodec.h>

// Globals
ALLEGRO_DISPLAY *display = NULL;
ALLEGRO_TIMER *timer = NULL;
ALLEGRO_EVENT_QUEUE *ev_queue = NULL;
ALLEGRO_PATH *config_path = NULL;
ALLEGRO_CONFIG *config = NULL;
ALLEGRO_FONT *font = NULL;
ALLEGRO_JOYSTICK *gamepad = NULL;

ALLEGRO_BITMAP *left_hand = NULL;
ALLEGRO_BITMAP *right_hand = NULL;
ALLEGRO_BITMAP *left_hand_shadow = NULL;
ALLEGRO_BITMAP *right_hand_shadow = NULL;
ALLEGRO_BITMAP *joints = NULL;
ALLEGRO_BITMAP *background = NULL;
ALLEGRO_BITMAP *heart = NULL;

ALLEGRO_BITMAP *dead_bg = NULL;
ALLEGRO_BITMAP *endscene_bg = NULL;
ALLEGRO_BITMAP *endscene_sprites = NULL;

ALLEGRO_AUDIO_STREAM *music = NULL;

ALLEGRO_SAMPLE *hurt = NULL;
ALLEGRO_SAMPLE *dead = NULL;
ALLEGRO_SAMPLE *boom = NULL;
ALLEGRO_SAMPLE *failure = NULL;
ALLEGRO_SAMPLE *success = NULL;

ALLEGRO_TEXTLOG *textlog = NULL;

struct axis    left_stick_v, left_stick_h, right_stick_v, right_stick_h;
struct button  left_bottom_button, right_bottom_button, a_button, b_button;

// Initialises Allegro and required subsystems, returns true on success
static bool init_allegro(void) {
	if (!al_init()) {
		show_error("Failed to initialize allegro!");
		return false;
	}

	al_set_app_name(APP_NAME);

	textlog = al_open_native_text_log(APP_NAME, ALLEGRO_TEXTLOG_MONOSPACE);

	if (!al_install_keyboard()) {
		printlog("Error: failed to initialize the keyboard!\n");
		return false;
	}

	// Joystick required!
	if(!al_install_joystick()) {
		printlog("Error: failed to initialize the gamepad!\n");
		return false;
	}

	if (!al_init_image_addon()) {
		printlog("Error: failed to initialize image addon!\n");
		return false;
	}

	if (!al_init_font_addon()) {
		printlog("Error: failed to initialize font addon!\n");
		return false;
	}

	if (!al_init_primitives_addon()) {
		printlog("Error: failed to initialize primitives addon!\n");
		return false;
	}

	if (!al_install_audio()) {
		printlog("Error: failed to initialize audio addon!\n");
		return false;
	}

	if (!al_init_acodec_addon()) {
		printlog("Error: failed to initialize audio codec addon!\n");
		return false;
	}

	if (!al_reserve_samples(3)) {
		printlog("Error: failed to reserve audio samples!\n");
		return false;
	}

	if (ev_queue = al_create_event_queue(), !ev_queue) {
		printlog("Error: failed to create an event queue!\n");
		return false;
	}

	if (timer = al_create_timer(1.0/FPS), !timer) {
		printlog("Error: failed to initialise the timer!\n");
		return false;
	}

	al_set_new_display_flags(ALLEGRO_OPENGL | ALLEGRO_RESIZABLE);
	if (display = al_create_display(1000, 600), !display) {
		printlog("Error: failed to create the display!\n");
		return false;
	}

#ifndef NDEBUG
	if (al_install_mouse()) {
		al_set_system_mouse_cursor(display, ALLEGRO_SYSTEM_MOUSE_CURSOR_PRECISION);
	}
#endif

	al_register_event_source(ev_queue, al_get_timer_event_source(timer));
	al_register_event_source(ev_queue, al_get_display_event_source(display));
	al_register_event_source(ev_queue, al_get_joystick_event_source());
	al_register_event_source(ev_queue, al_get_keyboard_event_source());

	printlog("Allegro backend initialised with no error!\n");
	return true;
}

// returns binding from config, sets default value if not set
static struct button get_button(const char *name, struct button defaults, bool *conf_modified) {
	const char *val = al_get_config_value(config, NULL, name);
	struct button res = {0, 0};
	if (val != NULL && sscanf(val, "%2d,%2d", &(res.joy), &(res.button)) == 2) {
		return res;
	}
	else {
		config_set_bind_button(name, defaults);
		*conf_modified = true;
		return defaults;
	}
}

static struct axis get_axis(const char *name, struct axis defaults, bool *conf_modified) {
	const char *val = al_get_config_value(config, NULL, name);
	struct axis res = {0, 0, 0};
	if (val != NULL && sscanf(val, "%2d,%2d,%2d", &(res.joy), &(res.stick), &(res.axis)) == 3) {
		return res;
	}
	else {
		config_set_bind_axis(name, defaults);
		*conf_modified = true;
		return defaults;
	}
}

// key bindings
static bool load_config() {
	config_path = al_get_standard_path(ALLEGRO_USER_SETTINGS_PATH);

	ALLEGRO_FS_ENTRY *conf_dir = al_create_fs_entry(al_path_cstr(config_path, '/'));
	if (!al_fs_entry_exists(conf_dir)) {
		al_make_directory(al_path_cstr(config_path, '/'));
	}
	al_destroy_fs_entry(conf_dir);

	al_set_path_filename(config_path, "config.ini");
	printlogf("loading config file from %s\n", al_path_cstr(config_path, '/'));
	if (al_filename_exists(al_path_cstr(config_path, '/'))) {
		config = al_load_config_file(al_path_cstr(config_path, '/'));
	}
	else {
		config = al_create_config();
	}

	// bindings (stick, axis, button indices)
	// default values are those of an XInput gamepad
	bool conf_modified = false;
#ifdef WIN32
	left_stick_v  = get_axis( LEFT_STICK_V, (struct axis){ 0, 0, 1 }, &conf_modified);
	left_stick_h  = get_axis( LEFT_STICK_H, (struct axis){ 0, 0, 0 }, &conf_modified);
	right_stick_v = get_axis(RIGHT_STICK_V, (struct axis){ 0, 1, 1 }, &conf_modified);
	right_stick_h = get_axis(RIGHT_STICK_H, (struct axis){ 0, 1, 0 }, &conf_modified);
	left_bottom_button  = get_button( LEFT_BOTTOM_BUTTON, (struct button){ 0, 4 }, &conf_modified);
	right_bottom_button = get_button(RIGHT_BOTTOM_BUTTON, (struct button){ 0, 5 }, &conf_modified);
	a_button = get_button(A_BUTTON, (struct button) { 0, 0 }, &conf_modified);
	b_button = get_button(B_BUTTON, (struct button) { 0, 1 }, &conf_modified);
#else
	left_stick_v  = get_axis( LEFT_STICK_V, (struct axis) { 0, 0, 1 }, &conf_modified);
	left_stick_h  = get_axis( LEFT_STICK_H, (struct axis) { 0, 0, 0 }, &conf_modified);
	right_stick_v = get_axis(RIGHT_STICK_V, (struct axis) { 0, 2, 0 }, &conf_modified);
	right_stick_h = get_axis(RIGHT_STICK_H, (struct axis) { 0, 1, 1 }, &conf_modified);
	left_bottom_button  = get_button( LEFT_BOTTOM_BUTTON, (struct button) { 0, 4 }, &conf_modified);
	right_bottom_button = get_button(RIGHT_BOTTOM_BUTTON, (struct button) { 0, 5 }, &conf_modified);
	a_button = get_button(A_BUTTON, (struct button) { 0, 0 }, &conf_modified);
	b_button = get_button(B_BUTTON, (struct button) { 0, 1 }, &conf_modified);
#endif

	if (conf_modified) {
		bool test = al_save_config_file(al_path_cstr(config_path, '/'), config);
		if (test) {
			printlogf("saved config file into %s\n", al_path_cstr(config_path, '/'));
		}
		else {
			printlogf("failed to save config file into %s\n", al_path_cstr(config_path, '/'));
		}
	}

	return true;
}

static void draw_gamepad_plug_message(ALLEGRO_BITMAP *gamepad32) {
	al_clear_to_color(al_map_rgb(0,0,0));

	int screen_cx = al_get_display_width(display)  / 2.,
	    screen_cy = al_get_display_height(display) / 2.;

	if (gamepad32) al_draw_scaled_bitmap(gamepad32, 0, 0, 32, 32, screen_cx-244, screen_cy - 64, 128, 128, 0);

	draw_scaled_text(font, al_map_rgb(255,255,255), screen_cx-84, screen_cy, 4, 0, "Plug me in!");

	al_flip_display();
}

// Checks if a gamepad is available, returns false if there is no gamepad
static bool check_gamepad() {
	if (al_get_num_joysticks() < 1) {
		ALLEGRO_BITMAP *gamepad32 = get_resource_image("gamepad32.png");
		draw_gamepad_plug_message(gamepad32);

		while (al_get_num_joysticks() < 1) {
			printlog("Error: no gamepad detected, a gamepad is required to play this game!\n");

			ALLEGRO_EVENT ev;
			while (al_wait_for_event(ev_queue, &ev), true) {
				if (ev.type == ALLEGRO_EVENT_JOYSTICK_CONFIGURATION ||
				    ev.type == ALLEGRO_EVENT_JOYSTICK_AXIS ||
				    ev.type == ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN ||
				    ev.type == ALLEGRO_EVENT_JOYSTICK_BUTTON_UP ||
				    ev.type == ALLEGRO_EVENT_KEY_DOWN) {
					break;
				}
				if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
					return false;
				}
				if (ev.type == ALLEGRO_EVENT_DISPLAY_SWITCH_IN) {
					draw_gamepad_plug_message(gamepad32);
				}
				if (ev.type == ALLEGRO_EVENT_DISPLAY_RESIZE) {
					al_acknowledge_resize(display);
					draw_gamepad_plug_message(gamepad32);
				}
			}
			al_reconfigure_joysticks();
		}

		if (gamepad32) al_destroy_bitmap(gamepad32);
	}

	gamepad = al_get_joystick(0);
	printlogf("Gamepad detected: %s\n", al_get_joystick_name(gamepad));

	return true;
}

// Images, sounds, ...
static bool load_resources() {

	if (font = al_create_builtin_font(), !font) {
		printlog("Error: failed to create the font!\n");
		return false;
	}

	if (cursor = get_resource_image("cursor.png"), !cursor) {
		printlog("Error: failed to load cursor.png!\n");
		return false;
	}

	if (menu_bg = get_resource_image("oscillo.png"), !menu_bg) {
		printlog("Error: failed to load oscillo.png!\n");
		return false;
	}

	if (conf_gamepad = get_resource_image("gamepad.png"), !conf_gamepad) {
		printlog("Error: failed to load gamepad.png!\n");
		return false;
	}

	if (left_hand = get_resource_image("left_hand.png"), !left_hand) {
		printlog("Error: failed to load left_hand.png");
		return false;
	}

	if (right_hand = get_resource_image("right_hand.png"), !left_hand) {
		printlog("Error: failed to load right_hand.png");
		return false;
	}

	if (left_hand_shadow = get_resource_image("left_hand_shadow.png"), !left_hand) {
		printlog("Error: failed to load left_hand_shadow.png");
		return false;
	}

	if (right_hand_shadow = get_resource_image("right_hand_shadow.png"), !left_hand) {
		printlog("Error: failed to load right_hand_shadow.png");
		return false;
	}

	if (joints = get_resource_image("joints.png"), !joints) {
		printlog("Error: failed to load joints.png");
		return false;
	}

	if (heart = get_resource_image("heart.png"), !heart) {
		printlog("Error: failed to load heart.png");
		return false;
	}

	if (dead_bg = get_resource_image("dead.png"), !heart) {
		printlog("Error: failed to load dead.png");
		return false;
	}

	if (endscene_bg = get_resource_image("endscene.png"), !heart) {
		printlog("Error: failed to load endscene.png");
		return false;
	}

	if (endscene_sprites = get_resource_image("end_sprites.png"), !heart) {
		printlog("Error: failed to load end_sprites.png");
		return false;
	}

	if (( (int)(al_get_time()*1000) ) %2) {
		if (hurt = get_resource_sample("tmnath.ogg"), !hurt) {
			printlog("Error: failed to load tmnath.ogg");
			return false;
		}
	}
	else {
		if (hurt = get_resource_sample("marcadet.ogg"), !hurt) {
			printlog("Error: failed to load marcadet.ogg");
			return false;
		}
	}

	if (dead = get_resource_sample("dead.ogg"), !dead) {
		printlog("Error: failed to load dead.ogg");
		return false;
	}

	if (boom = get_resource_sample("boom.ogg"), !boom) {
		printlog("Error: failed to load boom.ogg");
		return false;
	}

	if (failure = get_resource_sample("failure.ogg"), !failure) {
		printlog("Error: failed to load failure.ogg");
		return false;
	}

	if (success = get_resource_sample("success.ogg"), !success) {
		printlog("Error: failed to load success.ogg");
		return false;
	}

	if (music = get_resource_stream("music.xm"), !music) {
		printlog("Error: failed to load music.xm");
		return false;
	}
	al_set_audio_stream_playmode(music, ALLEGRO_PLAYMODE_LOOP);

	printlog("All resources loaded!\n");

	return true;
}

bool init() {
	return init_allegro() && load_config() && load_resources() && check_gamepad();
}

// Returns when the `textlog` window is closed
static void wait_for_textlog() {
	if (ev_queue = al_create_event_queue(), !ev_queue) {
		printlog("Cannot create event queue to wait for textlog termination, resting 10 seconds...\n");
		al_rest(10.);
		return;
	}
	al_register_event_source(ev_queue, al_get_native_text_log_event_source(textlog));
	ALLEGRO_EVENT ev;
	do {
		al_wait_for_event(ev_queue, &ev);
	} while (ev.type != ALLEGRO_EVENT_NATIVE_DIALOG_CLOSE);
	al_destroy_event_queue(ev_queue);
}

// Deallocates, deinitialises
void deinit(void) {
	printlog("Releasing all resources\n");
	if (music) al_destroy_audio_stream(music);

	if (success) al_destroy_sample(success);
	if (failure) al_destroy_sample(failure);

	if (dead) al_destroy_sample(dead);
	if (hurt) al_destroy_sample(hurt);

	if (endscene_sprites) al_destroy_bitmap(endscene_sprites);
	if (endscene_bg)      al_destroy_bitmap(endscene_bg);

	if (dead_bg) al_destroy_bitmap(dead_bg);
	if ( joints) al_destroy_bitmap( joints);

	if ( left_hand_shadow) al_destroy_bitmap( left_hand_shadow);
	if (right_hand_shadow) al_destroy_bitmap(right_hand_shadow);
	if ( left_hand) al_destroy_bitmap( left_hand);
	if (right_hand) al_destroy_bitmap(right_hand);

	if (menu_bg) al_destroy_bitmap(menu_bg);
	if (cursor)  al_destroy_bitmap(cursor);
	if (font)    al_destroy_font(font);

	if (config)      al_destroy_config(config);
	if (config_path) al_destroy_path(config_path);
	if (display)     al_destroy_display(display);
	if (timer)       al_destroy_timer(timer);
	if (ev_queue)    al_destroy_event_queue(ev_queue);
	if (textlog) {
		wait_for_textlog();
		al_close_native_text_log(textlog);
	}
	al_uninstall_system();
}

