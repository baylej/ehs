#include "game.h"

level_data current_level = {0};

// Format = 0x0NNBB       NN = track unique identifier    BB = BitField: properties of cell
int grid[] = {
	0x0105, 0x0101, 0x0101, 0x0101, 0x0101, 0x0101, 0x0101, 0x0101, 0x0105, 0x0000, 0x0000, 0x0205, 0x0201, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0101, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0201, 0x0201, 0x0201, 0x0201, 0x0201, 0x0201, 0x0201,
	0x0000, 0x0000, 0x0101, 0x0101, 0x0101, 0x0101, 0x0105, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0205, 0x0000, 0x0000, 0x0201, 0x0000, 0x0000, 0x0201,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0201, 0x0000, 0x0000, 0x0201,
	0x0405, 0x0401, 0x0401, 0x0401, 0x0401, 0x0401, 0x0405, 0x0000, 0x0000, 0x0301, 0x0301, 0x0301, 0x0305, 0x0301, 0x0305, 0x0205, 0x0000, 0x0000, 0x0205,
	0x0401, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0301, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0005,
	0x0405, 0x0000, 0x0000, 0x0000, 0x0305, 0x0301, 0x0305, 0x0301, 0x0301, 0x0301, 0x0000, 0x0000, 0x0505, 0x0501, 0x0505, 0x0000, 0x0000, 0x0000, 0x0005,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
	0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0605, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0705, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000
};

int holes[20][2] = {
	{0, 0}, {8, 0}, {11, 0},
	{6, 2}, {12, 2},
	{0, 4}, {6, 4}, {12, 4}, {14, 4}, {15, 4}, {18, 4},
	{18, 5},
	{0, 6}, {4, 6}, {6, 6}, {12, 6}, {14, 6}, {18, 6},
	{6, 8}, {12, 8}
};

bool load_level(int index) {
	printlogf("Loading level %d\n", index);

	memset(&current_level, 0, sizeof(level_data));

	current_level.w = 19;
	current_level.h =  9;

	current_level.grid = grid;

	current_level.holes_len = 20;
	current_level.holes = holes;

	current_level.pcb = get_resource_image("blinky_pcb_back.png");
	if (!(current_level.pcb)) {
		printlog("ERROR: Level loading failed on asset loading!\n");
		return false;
	}

	int pcb_h = al_get_bitmap_height(current_level.pcb), pcb_w = al_get_bitmap_width(current_level.pcb);
	current_level.pcb_f = (GA_H - 2*GA_PAD_H) / (float)pcb_h;
	if (current_level.pcb_f * pcb_w > (GA_W - 2*GA_PAD_W)) {
		current_level.pcb_f = (GA_W - 2*GA_PAD_W) / (float)pcb_w;
	}

	return true;
}

void destroy_level() {
	for (int i=0; i<current_level.h*current_level.w; i++) {
		current_level.grid[i] = current_level.grid[i] & 0xFF05;
	}
	al_destroy_bitmap(current_level.pcb);
}

int get_cell(int cell_x, int cell_y) {
	int res = 0;

	if (cell_x >= 0 && cell_x < current_level.w &&
	    cell_y >= 0 && cell_y < current_level.h) {
		res = *(current_level.grid + cell_x + cell_y*current_level.w);
	}

	return res;
}

void set_cell(int cell_x, int cell_y, int val) {
	if (cell_x >= 0 && cell_x < current_level.w &&
		cell_y >= 0 && cell_y < current_level.h) {
		*(current_level.grid + cell_x + cell_y*current_level.w) = val;
	}
}

#define IS_JOINT(x) (x & JOINT)
#define IS_TRACK(x) (x & TRACK)
#define IS_OVERFLOW(x) (x & OVERFLOW)
#define GET_TRACK_ID(x) (x>>8)

static void set_mark(bool *mark, int cell_x, int cell_y) {
	if (cell_x >= 0 && cell_x < current_level.w &&
	    cell_y >= 0 && cell_y < current_level.h) {
		mark[cell_x + cell_y*current_level.w] = true;
	}
}

static bool get_mark(bool *mark, int cell_x, int cell_y) {
	if (cell_x >= 0 && cell_x < current_level.w &&
	    cell_y >= 0 && cell_y < current_level.h) {
		return mark[cell_x + cell_y*current_level.w];
	}
	return true;
}

// Add neighbouring cells that make a path with cell at given location
// returns count of cells added to path
static int follow_path(int cell_x, int cell_y, bool *mark, int (*stack)[2]) {
	int i, res=0, cell[4][3], mid;

	mid = get_cell(cell_x, cell_y);
	cell[0][0] = cell_x-1;  cell[0][1] = cell_y;    cell[0][2] = get_cell(cell[0][0], cell[0][1]);
	cell[1][0] = cell_x+1;  cell[1][1] = cell_y;    cell[1][2] = get_cell(cell[1][0], cell[1][1]);
	cell[2][0] = cell_x;    cell[2][1] = cell_y-1;  cell[2][2] = get_cell(cell[2][0], cell[2][1]);
	cell[3][0] = cell_x;    cell[3][1] = cell_y+1;  cell[3][2] = get_cell(cell[3][0], cell[3][1]);

	for (i=0; i<4; i++) {

		if (!get_mark(mark, cell[i][0], cell[i][1]) &&
		      (IS_OVERFLOW(cell[i][2]) || (IS_OVERFLOW(mid) && IS_JOINT(cell[i][2])))) {
			set_mark(mark, cell[i][0], cell[i][1]);
			stack[res][0] = cell[i][0];
			stack[res][1] = cell[i][1];
			res++;
		}
	}
	return res;
}

bool check_for_shorts() {
	int i, j, track_id=0, cell;
	bool res = false;
	// mark, to remember which cells have already been visited
	bool *mark = calloc(current_level.w * current_level.h, sizeof(bool));
	int stack[256][2]; // Stack of grid coordinates
	int start, end; // start/end positions of the stack

	for (i=0; i < current_level.w; i++) {
		for (j=0; j < current_level.h; j++) {

			cell = get_cell(i, j);
			if (IS_JOINT(cell) && !get_mark(mark, i, j)) {
				set_mark(mark, i, j);
				stack[0][0] = i;
				stack[0][1] = j;
				end = 1;
			}
			else {
				continue;
			}

			// Follow solder track
			for (start=0; start<end && end<253; start++) {
				end += follow_path(stack[start][0], stack[start][1], mark, stack+end);
			}

			// Check for short in solder track
			printlogf("solder track found at cell (%d, %d), it is %d cells long\n", i, j, end);
			track_id = 0;
			for (start=0; start<end; start++) {
				cell = get_cell(stack[start][0], stack[start][1]);
				if (IS_TRACK(cell)) {
					if (track_id == 0) {
						track_id = GET_TRACK_ID(cell);
					}
					else if (track_id != GET_TRACK_ID(cell)) {
						res = true;
						printlogf("short found between track#%d and track#%d\n", track_id, GET_TRACK_ID(cell));
						goto end_func;
					}
				}
			}
		}
	}
end_func:
	free(mark);
	return res;
}

