#include "game.h"

int main(void) {
	if (!init()) goto quit;
	printlog("Game initialized!\n");

	printlog("Entering main menu ...\n");
	main_menu();

quit:
	deinit();

	return 0;
}

