#include "game.h"

segment translate(const segment *s, const point *v) {
	segment res;
	res.A.x = s->A.x + v->x;
	res.A.y = s->A.y + v->y;
	res.B.x = s->B.x + v->x;
	res.B.y = s->B.y + v->y;
	return res;
}

static inline point as_vect(const point a, const point b) {
	return (point) {a.x - b.x, a.y - b.y};
}

// 2D cross product (returns ~ tan())
static inline float cp(const point a, const point b) {
	return a.x * b.y - a.y * b.x;
}

bool intersect(const segment *s1, const segment *s2) {
	point AB = as_vect(s1->A, s1->B);
	point AD = as_vect(s1->A, s2->B);

	point BA = as_vect(s1->B, s1->A);
	point BD = as_vect(s1->B, s2->B);

	float AD_AB = cp(AD, AB);
	float BA_BD = cp(BA, BD);

	point CD = as_vect(s2->A, s2->B);
	point CB = as_vect(s2->A, s1->B);

	point DB = as_vect(s2->B, s1->B);
	point DC = as_vect(s2->B, s2->A);

	float CD_CB = cp(CD, CB);
	float DB_DC = cp(DB, DC);

	point DA = as_vect(s2->B, s1->A);

	float DA_DC = cp(DA, DC);

	return AD_AB < 0. && BA_BD < 0. && CD_CB < 0. && DB_DC < 0. && DA_DC > 0.;
}

