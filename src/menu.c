#include "game.h"

#include <allegro5/allegro_primitives.h>

ALLEGRO_BITMAP *cursor = NULL;
ALLEGRO_BITMAP *menu_bg = NULL;
ALLEGRO_BITMAP *conf_gamepad = NULL;

typedef enum _mme{ PLAY, CONFIG, QUIT } main_menu_entry;

static inline main_menu_entry main_next_entry(main_menu_entry e) {
	return (e + 1) % 3;
}
static inline main_menu_entry main_prev_entry(main_menu_entry e) {
	return (e + 2) % 3;
}

static void disp_main_menu(main_menu_entry selected_entry) {
	// screen's (x,y) / (800,600) ratio
	point factor = get_screen_factor();
	// as we will apply factor.x to both horizontal and vertical values
	// (because nowadays we mostly use wide screens), I have to compute
	// an horizontal offset to cernter drawing operations
	float xoffset = ((800*factor.x)-(800*factor.y)) / 2.;

	// top-left coordinates of the background
	float top = 120 * factor.y;
	float left = 58 * factor.y + xoffset;

	al_clear_to_color(al_map_rgb(0,0,0));

	al_draw_scaled_bitmap(menu_bg, 0, 0, 171, 90, left, top, 684*factor.y, 360*factor.y, 0);

	float font_left = left + 72 * 4*factor.y;

	ALLEGRO_COLOR col = al_map_rgb_f(1., 1., 1.);
	draw_scaled_text(font, col, font_left, top + 20 * 4*factor.y, 3*factor.y, ALLEGRO_ALIGN_RIGHT, "Play !");
	draw_scaled_text(font, col, font_left, top + 31 * 4*factor.y, 3*factor.y, ALLEGRO_ALIGN_RIGHT, "Options");
	draw_scaled_text(font, col, font_left, top + 42 * 4*factor.y, 3*factor.y, ALLEGRO_ALIGN_RIGHT, "Quit");

	float cursor_left = left + 101 * 4*factor.y;
	float cursor_top = top;

	switch (selected_entry) {
		case PLAY:    cursor_top += 24 * 4*factor.y; break;
		case CONFIG:  cursor_top += 35 * 4*factor.y; break;
		case QUIT:    cursor_top += 46 * 4*factor.y; break;
	}
	al_draw_scaled_rotated_bitmap(cursor, 11, 13, cursor_left, cursor_top, 2*factor.y, 2*factor.y, -ALLEGRO_PI/4., 0);
}

void main_menu() {
	ALLEGRO_EVENT ev;
	main_menu_entry selected = PLAY;
	bool continue_menuloop = true;

	float gamepad_vert_axis = 0.;

	al_start_timer(timer);
	do {
		al_wait_for_event(ev_queue, &ev);
		if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
			continue_menuloop = false;
		}
		else if (ev.type == ALLEGRO_EVENT_DISPLAY_RESIZE) {
			al_acknowledge_resize(display);
		}
		else if (ev.type == ALLEGRO_EVENT_JOYSTICK_AXIS) {
			if (ev.joystick.id    == gamepad &&
			    ev.joystick.stick == left_stick_v.stick &&
			    ev.joystick.axis  == left_stick_v.axis) {
				float new_axis = ev.joystick.pos;

				if (new_axis >= 0.5 && gamepad_vert_axis <= 0.1) {
					selected = main_next_entry(selected);
					gamepad_vert_axis = new_axis;
				}
				else if (new_axis <= -0.5 && gamepad_vert_axis >= -0.1) {
					selected = main_prev_entry(selected);
					gamepad_vert_axis = new_axis;
				}
				else if (new_axis < 0.1 && new_axis > -0.1) {
					gamepad_vert_axis = 0.;
				}
			}
		}
		else if (ev.type == ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN) {
			if (ev.joystick.id == gamepad && ev.joystick.button == a_button.button) {
				if (selected == QUIT) {
					continue_menuloop = false;
				}
				else if (selected == CONFIG) {
					config_gamepad_menu();
				}
				else if (selected == PLAY) {
					select_level();
				}
			}
		}
		else if (ev.type == ALLEGRO_EVENT_KEY_DOWN) {
			if (ev.keyboard.keycode == ALLEGRO_KEY_DOWN) {
				selected = main_next_entry(selected);
			}
			else if (ev.keyboard.keycode == ALLEGRO_KEY_UP) {
				selected = main_prev_entry(selected);
			}
			else if (ev.keyboard.keycode == ALLEGRO_KEY_ENTER) {
				if (selected == QUIT) {
					continue_menuloop = false;
				}
				else if (selected == CONFIG) {
					config_gamepad_menu();
				}
				else if (selected == PLAY) {
					select_level();
				}
			}
		}
		else if (ev.type == ALLEGRO_EVENT_TIMER) {
			disp_main_menu(selected);
			al_flip_display();
		}
	} while (continue_menuloop);
	al_stop_timer(timer);
}

/////////////////////////////////////////////////

typedef enum{L_AXIS_X, L_AXIS_Y, LB_BUTTON, RB_BUTTON, _B_BUTTON, _A_BUTTON, R_AXIS_X, R_AXIS_Y} config_menu_entry;

static inline config_menu_entry config_next_entry(config_menu_entry e) {
	return (e + 1) % 8;
}
static inline config_menu_entry config_prev_entry(config_menu_entry e) {
	return (e + 7) % 8;
}
static inline bool is_button(config_menu_entry e) {
	return e==LB_BUTTON || e==RB_BUTTON || e==_B_BUTTON || e==_A_BUTTON;
}

static void display_config_gamepad_menu(config_menu_entry selected, bool edit_active) {
	point factor = get_screen_factor();
	float xoffset = ((800*factor.x) - (800*factor.y)) / 2.;

	// top-left coordinates of the background
	float top  = 144*factor.y;
	float left = 142*factor.y + xoffset;

	al_clear_to_color(al_map_rgb(0, 0, 0));

	al_draw_scaled_bitmap(conf_gamepad, 0, 0, 86, 52, left, top, 516*factor.y, 312*factor.y, 0);

	ALLEGRO_JOYSTICK_STATE gamepad_state;
	al_get_joystick_state(gamepad, &gamepad_state);

	// buttons
	ALLEGRO_COLOR col = al_map_rgb(100, 40, 0);
	if (gamepad_state.button[a_button.button] > 16384) {
		al_draw_filled_circle(414*factor.y + left, 138*factor.y + top, 12*factor.y, col);
	}
	if (gamepad_state.button[b_button.button] > 16384) {
		al_draw_filled_circle(450*factor.y + left, 96*factor.y + top, 12*factor.y, col);
	}
	if (gamepad_state.button[left_bottom_button.button] > 16384) {
		al_draw_filled_circle(126*factor.y + left, top - 6*factor.y, 12*factor.y, col);
	}
	if (gamepad_state.button[right_bottom_button.button] > 16384) {
		al_draw_filled_circle(390*factor.y + left, top - 6*factor.y, 12*factor.y, col);
	}

	float axis_x, axis_y;

	// left stick
	axis_x = 27 * factor.y * (gamepad_state.stick[left_stick_h.stick].axis[left_stick_h.axis]);
	axis_y = 27 * factor.y * (gamepad_state.stick[left_stick_v.stick].axis[left_stick_v.axis]);
	axis_x = axis_x + left + 117*factor.y;
	axis_y = axis_y +  top +  75*factor.y;
	al_draw_filled_circle(axis_x, axis_y, 6*factor.y, col);

	// right stick
	axis_x = 27 * factor.y * (gamepad_state.stick[right_stick_h.stick].axis[right_stick_h.axis]);
	axis_y = 27 * factor.y * (gamepad_state.stick[right_stick_v.stick].axis[right_stick_v.axis]);
	axis_x = axis_x + left + 321*factor.y;
	axis_y = axis_y +  top + 153*factor.y;
	al_draw_filled_circle(axis_x, axis_y, 6*factor.y, col);

	col = al_map_rgb(225,80,0);
	// labels
	al_draw_line(left + 36*factor.y, top + 36*factor.y, left + 78*factor.y, top + 72*factor.y, col, 4.);
	draw_scaled_text(font, col, left + 36*factor.y, top + 36*factor.y, 1.5*factor.y, ALLEGRO_ALIGN_RIGHT, "Left stick horizontal");

	al_draw_line(left + 24*factor.y, top + 150*factor.y, left + 114*factor.y, top + 114*factor.y, col, 4.);
	draw_scaled_text(font, col, left + 24*factor.y, top + 150*factor.y, 1.5*factor.y, ALLEGRO_ALIGN_RIGHT, "Left stick vertical");

	al_draw_line(left + 354*factor.y, top + 234*factor.y, left + 318*factor.y, top + 192*factor.y, col, 4.);
	draw_scaled_text(font, col, left + 358*factor.y, top + 234*factor.y, 1.5*factor.y, 0, "Right stick vertical");

	al_draw_line(left + 396*factor.y, top + 192*factor.y, left + 360*factor.y, top + 150*factor.y, col, 4.);
	draw_scaled_text(font, col, left + 402*factor.y, top + 192*factor.y, 1.5*factor.y, 0, "Right stick horizontal");

	al_draw_line(left + 522*factor.y, top + 156*factor.y, left + 402*factor.y, top + 156*factor.y, col, 4.);
	draw_scaled_text(font, col, left + 528*factor.y, top + 156*factor.y, 1.5*factor.y, 0, "A button");

	al_draw_line(left + 522*factor.y, top + 114*factor.y, left + 438*factor.y, top + 114*factor.y, col, 4.);
	draw_scaled_text(font, col, left + 528*factor.y, top + 114*factor.y, 1.5*factor.y, 0, "B button");

	al_draw_line(left + 196*factor.y, top - 30*factor.y, left + 156*factor.y, top, col, 4.);
	draw_scaled_text(font, col, left + 202*factor.y, top - 30*factor.y, 1.5*factor.y, 0, "LB button");

	al_draw_line(left + 474*factor.y, top - 30*factor.y, left + 432*factor.y, top, col, 4.);
	draw_scaled_text(font, col, left + 480*factor.y, top - 30*factor.y, 1.5*factor.y, 0, "RB button");

	float cursor_left  = left;
	float cursor_top   = top;
	float cursor_angle = 0.;

	switch (selected) {
		case L_AXIS_X:  cursor_left -= 108*factor.y;  cursor_top +=  66*factor.y;  cursor_angle =  ALLEGRO_PI/4.; break;
		case L_AXIS_Y:  cursor_left -=  96*factor.y;  cursor_top += 180*factor.y;  cursor_angle =  ALLEGRO_PI/4.; break;
		case LB_BUTTON: cursor_left += 258*factor.y; break;
		case RB_BUTTON: cursor_left += 534*factor.y; break;
		case R_AXIS_Y:  cursor_left += 594*factor.y;  cursor_top += 258*factor.y;  cursor_angle = -ALLEGRO_PI/4.; break;
		case R_AXIS_X:  cursor_left += 642*factor.y;  cursor_top += 216*factor.y;  cursor_angle = -ALLEGRO_PI/4.; break;
		case _A_BUTTON: cursor_left += 648*factor.y;  cursor_top += 150*factor.y;  cursor_angle = -ALLEGRO_PI/2.; break;
		case _B_BUTTON: cursor_left += 648*factor.y;  cursor_top += 108*factor.y;  cursor_angle = -ALLEGRO_PI/2.; break;
	}
	if (edit_active) {
		al_draw_tinted_scaled_rotated_bitmap(cursor, al_map_rgb(0,255,0),
		    11, 13,
		    cursor_left, cursor_top,
		    1.5*factor.y, 1.5*factor.y, cursor_angle, 0);
	}
	else {
		al_draw_scaled_rotated_bitmap(cursor,
		    11, 13,
		    cursor_left, cursor_top,
		    1.5*factor.y, 1.5*factor.y, cursor_angle, 0);
	}

	draw_scaled_text(font, al_map_rgb(255, 255, 255), xoffset + 400 * factor.y, 504 * factor.y, 2 * factor.y,
		ALLEGRO_ALIGN_CENTRE, "Press LEFT and RIGHT on your keyboard to navigate.");
	draw_scaled_text(font, al_map_rgb(255,255,255), xoffset+400*factor.y, 540*factor.y, 2*factor.y,
	    ALLEGRO_ALIGN_CENTRE, "Press ENTER to edit binding, press ESCAPE to quit.");
}

// Sets button and axis global objects, also sets values in the ALLEGRO_CONFIG global datastructure
static void set_selected_bind_to(config_menu_entry selected, ALLEGRO_EVENT *ev) {
	switch (selected) {
		case L_AXIS_X:
			printlogf("Left stick horizontal bound to stick#%d,%d\n", ev->joystick.stick, ev->joystick.axis);
			left_stick_h.stick = ev->joystick.stick;
			left_stick_h.axis  = ev->joystick.axis;
			config_set_bind_axis(LEFT_STICK_H, left_stick_h);
			break;
		case L_AXIS_Y:
			printlogf("Left stick vertical bound to stick#%d,%d\n", ev->joystick.stick, ev->joystick.axis);
			left_stick_v.stick = ev->joystick.stick;
			left_stick_v.axis  = ev->joystick.axis;
			config_set_bind_axis(LEFT_STICK_V, left_stick_v);
			break;
		case LB_BUTTON:
			printlogf("LB button bound to button#%d\n", ev->joystick.button);
			left_bottom_button.button = ev->joystick.button;
			config_set_bind_button(LEFT_BOTTOM_BUTTON, left_bottom_button);
			break;
		case RB_BUTTON:
			printlogf("RB button bound to button#%d\n", ev->joystick.button);
			right_bottom_button.button = ev->joystick.button;
			config_set_bind_button(RIGHT_BOTTOM_BUTTON, right_bottom_button);
			break;
		case R_AXIS_Y:
			printlogf("Right stick vertical bound to stick#%d,%d\n", ev->joystick.stick, ev->joystick.axis);
			right_stick_v.stick = ev->joystick.stick;
			right_stick_v.axis  = ev->joystick.axis;
			config_set_bind_axis(RIGHT_STICK_V, right_stick_v);
			break;
		case R_AXIS_X:
			printlogf("Right stick horizontal bound to stick#%d,%d\n", ev->joystick.stick, ev->joystick.axis);
			right_stick_h.stick = ev->joystick.stick;
			right_stick_h.axis  = ev->joystick.axis;
			config_set_bind_axis(RIGHT_STICK_H, right_stick_h);
			break;
		case _A_BUTTON:
			printlogf("A button bound to button#%d\n", ev->joystick.button);
			a_button.button = ev->joystick.button;
			config_set_bind_button(A_BUTTON, a_button);
			break;
		case _B_BUTTON:
			printlogf("B button bound to button#%d\n", ev->joystick.button);
			b_button.button = ev->joystick.button;
			config_set_bind_button(B_BUTTON, b_button);
			break;
	}
}

void config_gamepad_menu() {
	ALLEGRO_EVENT ev;
	config_menu_entry selected = L_AXIS_X;
	bool edit_active = false;
	bool config_changed = false;
	bool continue_menuloop = true;

	do {
		al_wait_for_event(ev_queue, &ev);
		if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
			continue_menuloop = false;
		}
		else if (ev.type == ALLEGRO_EVENT_DISPLAY_RESIZE) {
			al_acknowledge_resize(display);
		}
		else if (ev.type == ALLEGRO_EVENT_KEY_DOWN) {
			if (ev.keyboard.keycode == ALLEGRO_KEY_ESCAPE) {
				continue_menuloop = false;
			}
			if (ev.keyboard.keycode == ALLEGRO_KEY_RIGHT) {
				selected = config_next_entry(selected);
			}
			else if (ev.keyboard.keycode == ALLEGRO_KEY_LEFT) {
				selected = config_prev_entry(selected);
			}
			else if (ev.keyboard.keycode == ALLEGRO_KEY_ENTER) {
				edit_active = !edit_active;
			}
		}
		else if (ev.type == ALLEGRO_EVENT_JOYSTICK_BUTTON_DOWN && edit_active) {
			if (ev.joystick.id == gamepad && is_button(selected)) {
				set_selected_bind_to(selected, &ev);
				edit_active = false;
				config_changed = true;
			}
		}
		else if (ev.type == ALLEGRO_EVENT_JOYSTICK_AXIS && edit_active) {
			if (ev.joystick.id == gamepad && !is_button(selected) &&
			    (ev.joystick.pos > .8 || ev.joystick.pos < -.8)) {
				set_selected_bind_to(selected, &ev);
				edit_active = false;
				config_changed = true;
			}
		}
		else if (ev.type == ALLEGRO_EVENT_TIMER) {
			display_config_gamepad_menu(selected, edit_active);
			al_flip_display();
		}
	} while (continue_menuloop);

	if (config_changed) {
		bool test = al_save_config_file(al_path_cstr(config_path, '/'), config);
		if (test) {
			printlogf("saved config file into %s\n", al_path_cstr(config_path, '/'));
		}
		else {
			printlogf("failed to save config file into %s\n", al_path_cstr(config_path, '/'));
		}
	}
}

/////////////////////////////////////////////////

bool select_level() {
	// TODO: Level picker menu
	if (!load_level(0)) {
		return false;
	}
	if (!game()) {
		return false;
	}
	destroy_level();
	return true;
}

