#include "game.h"

#include <stdio.h>

// Constants
const char  *LEFT_STICK_V =  "left_stick_v";
const char  *LEFT_STICK_H =  "left_stick_h";
const char *RIGHT_STICK_V = "right_stick_v";
const char *RIGHT_STICK_H = "right_stick_h";

const char  *LEFT_BOTTOM_BUTTON =  "left_bottom_button";
const char *RIGHT_BOTTOM_BUTTON = "right_bottom_button";
const char *A_BUTTON = "a_button";
const char *B_BUTTON = "b_button";

void show_error(const char *errmsg) {
	al_show_native_message_box(NULL, "Error", "An error occured:", errmsg, NULL, ALLEGRO_MESSAGEBOX_ERROR);
}

void printlog(const char* msg) {
	al_append_native_text_log(textlog, msg);
}

void printlogf(const char *format, ...) {
	va_list args;
	va_start(args, format);
	vprintlogf(format, args);
	va_end(args);
}

void vprintlogf(const char*format, va_list args) {
	ALLEGRO_USTR *buf = al_ustr_dup(al_ustr_empty_string());
	al_ustr_vappendf(buf, format, args);
	al_append_native_text_log(textlog, al_cstr(buf));
	al_ustr_free(buf);
}

ALLEGRO_BITMAP* get_resource_image(const char *filename) {
	ALLEGRO_PATH *path;
	ALLEGRO_BITMAP *image;

	path = al_get_standard_path(ALLEGRO_RESOURCES_PATH);
	al_append_path_component(path, RESOURCES_DIR);
	al_set_path_filename(path, filename);

	image = al_load_bitmap(al_path_cstr(path, '/'));
	printlogf("loading resource image %s... %s\n", al_path_cstr(path, '/'), image? "success": "failure");

	al_destroy_path(path);
	return image;
}

ALLEGRO_SAMPLE* get_resource_sample(const char *filename) {
	ALLEGRO_PATH *path;
	ALLEGRO_SAMPLE *sample;

	path = al_get_standard_path(ALLEGRO_RESOURCES_PATH);
	al_append_path_component(path, RESOURCES_DIR);
	al_set_path_filename(path, filename);

	sample = al_load_sample(al_path_cstr(path, '/'));
	printlogf("loading resource sample %s... %s\n", al_path_cstr(path, '/'), sample? "success": "failure");

	al_destroy_path(path);
	return sample;
}

ALLEGRO_AUDIO_STREAM* get_resource_stream(const char *filename) {
	ALLEGRO_PATH *path;
	ALLEGRO_AUDIO_STREAM *stream;

	path = al_get_standard_path(ALLEGRO_RESOURCES_PATH);
	al_append_path_component(path, RESOURCES_DIR);
	al_set_path_filename(path, filename);

	stream = al_load_audio_stream(al_path_cstr(path, '/'), 4, 1024);
	printlogf("loading resource stream %s... %s\n", al_path_cstr(path, '/'), stream? "success": "failure");

	al_destroy_path(path);
	return stream;
}

void config_set_bind_button(const char *name, struct button values) {
	char str[6];
	snprintf(str, 6, "%d,%d", values.joy, values.button);
	al_set_config_value(config, NULL, name, str);
}

void config_set_bind_axis(const char *name, struct axis values) {
	char str[9];
	snprintf(str, 9, "%d,%d,%d", values.joy, values.stick, values.axis);
	al_set_config_value(config, NULL, name, str);
}

#ifndef NDEBUG
#include <allegro5/allegro_primitives.h>
#endif

void draw_scaled_ustr(const ALLEGRO_FONT *font, ALLEGRO_COLOR color,
		float x, float y, float scale, int flags, const ALLEGRO_USTR *ustr) {
	int v, w, h;
	al_get_ustr_dimensions(font, ustr, &v, &v, &w, &h);
	ALLEGRO_BITMAP *res = al_create_bitmap(w, h);
	if (!res) {
		printlogf("failed to create bitmap with dimensions: %d,%d\n", w, h);
		return;
	}

	ALLEGRO_BITMAP *save = al_get_target_bitmap();
	al_set_target_bitmap(res);
	al_clear_to_color(al_map_rgba(0,0,0,0));
	al_draw_ustr(font, color, 0, 0, 0, ustr);
	al_set_target_bitmap(save);

	if (flags & ALLEGRO_ALIGN_CENTRE) {
		x = x - (w*scale)/2.;
	}
	else if (flags & ALLEGRO_ALIGN_RIGHT) {
		x = x - w*scale;
	}
	y -= h*scale/2.;

	al_draw_scaled_bitmap(res, 0, 0, w, h, x, y, w*scale, h*scale, 0);
#ifndef NDEBUG
	al_draw_rectangle(x, y, x + w*scale, y + h*scale, al_map_rgb(255, 0, 0), 1.);
#endif
	al_destroy_bitmap(res);
}

void draw_scaled_text(const ALLEGRO_FONT *font, ALLEGRO_COLOR color,
		float x, float y, float scale, int flags, const char *text) {
	ALLEGRO_USTR_INFO info;
	draw_scaled_ustr(font, color, x, y, scale, flags, al_ref_cstr(&info, text));
}

point get_screen_factor() {
	point res;
	res.x = (float)al_get_display_width(display) / 800.;
	res.y = (float)al_get_display_height(display) / 600.;
	//res.y = res.x; // NO DEFORM
	return res;
}

void play_music() {
	al_attach_audio_stream_to_mixer(music, al_get_default_mixer());
}

void stop_music() {
	al_detach_audio_stream(music);
	al_rewind_audio_stream(music);
}

